terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.81.0"
    }
  }
}

variable "GCP_SERVICE_ACCOUNT" {
  type = string
}

variable "GCP_SERVICE_KEY" {
  type = string
}

variable "NEW_RELIC_LICENSE_KEY" {
  type    = string
  default = "e5e733e45dbcb8af58e68285b55160d8FFFFNRAL"
}

provider "google" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

provider "google-beta" {
  project     = "codecollab-io"
  region      = "us-central1"
  credentials = var.GCP_SERVICE_KEY
}

data "terraform_remote_state" "secret_manager" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces   = {
      name = "secret-manager-production"
    }
  }
}

data "terraform_remote_state" "iam" {
  backend = "remote"

  config = {
    organization = "codecollab-io"
    workspaces   = {
      name = "iam-production"
    }
  }
}

data "google_iam_policy" "no_auth" {
  binding {
    role    = "roles/run.invoker"
    members = [
      "allUsers"
    ]
  }
}
