#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||

resource "google_cloud_run_service" "cc_api" {
  provider = google-beta

  name                       = "cc-api"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api/root:latest"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-api"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_api" {
//   service  = google_cloud_run_service.cc_api.name
//   project  = google_cloud_run_service.cc_api.project
//   location = google_cloud_run_service.cc_api.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_api" {
  location = "us-central1"
  name     = "cc-api.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_api.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api.name
  }
}

#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."

resource "google_cloud_run_service" "cc_api_qa" {
  provider = google-beta

  name                       = "cc-api-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api/root:test"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-api-qa"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_qa_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_api_qa" {
//   service  = google_cloud_run_service.cc_api_qa.name
//   project  = google_cloud_run_service.cc_api_qa.project
//   location = google_cloud_run_service.cc_api_qa.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_api_qa" {
  location = "us-central1"
  name     = "cc-api-qa.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_api_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_qa.name
  }
}
