#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||

# $$\   $$\  $$$$$$\  
# $$ |  $$ |$$  __$$\ 
# $$ |  $$ |$$ /  \__|
# $$ |  $$ |\$$$$$$\  
# $$ |  $$ | \____$$\ 
# $$ |  $$ |$$\   $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

resource "google_cloud_run_service" "cc_editor_api_us" {
  provider = google-beta

  name                       = "cc-editor-api-us"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-editor-api/root:latest"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-editor-api-us"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "256Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_us_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_editor_api_us" {
//   service  = google_cloud_run_service.cc_editor_api_us.name
//   project  = google_cloud_run_service.cc_editor_api_us.project
//   location = google_cloud_run_service.cc_editor_api_us.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_editor_api_us" {
  location = "us-central1"
  name     = "cc-editor-api-us.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_editor_api_us.project
  }

  spec {
    route_name = google_cloud_run_service.cc_editor_api_us.name
  }
}


#   $$$$$$\   $$$$$$\  
# $$  __$$\ $$  __$$\ 
# $$ /  \__|$$ /  \__|
# \$$$$$$\  $$ |$$$$\ 
#  \____$$\ $$ |\_$$ |
# $$\   $$ |$$ |  $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

resource "google_cloud_run_service" "cc_editor_api_sg" {
  provider = google-beta

  name                       = "cc-editor-api-sg"
  location                   = "asia-southeast1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-editor-api/root:latest"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-editor-api-sg"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "256Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_sg_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_editor_api_sg" {
//   service  = google_cloud_run_service.cc_editor_api_sg.name
//   project  = google_cloud_run_service.cc_editor_api_sg.project
//   location = google_cloud_run_service.cc_editor_api_sg.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_editor_api_sg" {
  location = "asia-southeast1"
  name     = "cc-editor-api-sg.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_editor_api_sg.project
  }

  spec {
    route_name = google_cloud_run_service.cc_editor_api_sg.name
  }
}

#  $$$$$$$$\ $$\   $$\ 
# $$  _____|$$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$\    $$ |  $$ |
# $$  __|   $$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$$$$\ \$$$$$$  |
# \________| \______/

resource "google_cloud_run_service" "cc_editor_api_eu" {
  provider = google-beta

  name                       = "cc-editor-api-eu"
  location                   = "europe-west4"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-editor-api/root:latest"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-editor-api-eu"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "256Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_eu_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_editor_api_eu" {
//   service  = google_cloud_run_service.cc_editor_api_eu.name
//   project  = google_cloud_run_service.cc_editor_api_eu.project
//   location = google_cloud_run_service.cc_editor_api_eu.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_editor_api_eu" {
  location = "europe-west4"
  name     = "cc-editor-api-eu.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_editor_api_eu.project
  }

  spec {
    route_name = google_cloud_run_service.cc_editor_api_eu.name
  }
}

#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."
resource "google_cloud_run_service" "cc_editor_api_qa" {
  provider = google-beta

  name                       = "cc-editor-api-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-editor-api/root:test"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-editor-api-qa"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "256Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_editor_api_qa_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_editor_api_qa" {
//   service  = google_cloud_run_service.cc_editor_api_qa.name
//   project  = google_cloud_run_service.cc_editor_api_qa.project
//   location = google_cloud_run_service.cc_editor_api_qa.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_editor_api_qa" {
  location = "us-central1"
  name     = "cc-editor-api-qa.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_editor_api_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_editor_api_qa.name
  }
}
