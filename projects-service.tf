resource "google_cloud_run_service" "cc_projects_service_qa" {
  provider = google-beta

  name                       = "cc-projects-service-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-projects-service/root:test"

        ports {
          name           = "h2c"
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_projects_service_qa_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

resource "google_cloud_run_service_iam_policy" "cc_projects_service_qa" {
  service  = google_cloud_run_service.cc_projects_service_qa.name
  project  = google_cloud_run_service.cc_projects_service_qa.project
  location = google_cloud_run_service.cc_projects_service_qa.location

  policy_data = data.google_iam_policy.no_auth.policy_data
}

resource "google_cloud_run_domain_mapping" "cc_projects_service_qa" {
  location = "us-central1"
  name     = "${google_cloud_run_service.cc_projects_service_qa.name}.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_projects_service_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_projects_service_qa.name
  }
}
