#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||

resource "google_cloud_run_service" "cc_auth" {
  provider = google-beta

  name                       = "cc-auth"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-auth/root:latest"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-auth"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        volume_mounts {
          name       = "jwt-public-key"
          mount_path = "/node/bin/keys/public"
        }

        volume_mounts {
          name       = "jwt-private-key"
          mount_path = "/node/bin/keys/private"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_auth_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      volumes {
        name = "jwt-public-key"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.jwt_public_key_secret_id
          items {
            key  = "latest"
            path = ".key"
          }
        }
      }

      volumes {
        name = "jwt-private-key"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.jwt_private_key_secret_id
          items {
            key  = "latest"
            path = ".key"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_auth" {
//   service  = google_cloud_run_service.cc_auth.name
//   project  = google_cloud_run_service.cc_auth.project
//   location = google_cloud_run_service.cc_auth.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_auth" {
  location = "us-central1"
  name     = "${google_cloud_run_service.cc_auth.name}.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_auth.project
  }

  spec {
    route_name = google_cloud_run_service.cc_auth.name
  }
}


#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."

resource "google_cloud_run_service" "cc_auth_qa" {
  provider = google-beta

  name                       = "cc-auth-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-auth/root:test"

        ports {
          container_port = 8111
        }

        env {
          name  = "NEW_RELIC_APP_NAME"
          value = "cc-auth-qa"
        }

        env {
          name  = "NEW_RELIC_LICENSE_KEY"
          value = var.NEW_RELIC_LICENSE_KEY
        }

        volume_mounts {
          name       = "config"
          mount_path = "/node/bin/env"
        }

        volume_mounts {
          name       = "jwt-public-key-qa"
          mount_path = "/node/bin/keys/public"
        }

        volume_mounts {
          name       = "jwt-private-key-qa"
          mount_path = "/node/bin/keys/private"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_auth_qa_config_secret_id
          items {
            key  = "latest"
            path = ".env"
          }
        }
      }

      volumes {
        name = "jwt-public-key-qa"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.jwt_public_key_qa_secret_id
          items {
            key  = "latest"
            path = ".key"
          }
        }
      }

      volumes {
        name = "jwt-private-key-qa"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.jwt_private_key_qa_secret_id
          items {
            key  = "latest"
            path = ".key"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_auth_qa" {
//   service  = google_cloud_run_service.cc_auth_qa.name
//   project  = google_cloud_run_service.cc_auth_qa.project
//   location = google_cloud_run_service.cc_auth_qa.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_auth_qa" {
  location = "us-central1"
  name     = "${google_cloud_run_service.cc_auth_qa.name}.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_auth_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_auth_qa.name
  }
}
