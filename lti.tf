#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||

resource "google_cloud_run_service" "cc_lti" {
  provider = google-beta

  name                       = "cc-lti"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-lti/root:latest"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_lti_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_lti" {
//   service  = google_cloud_run_service.cc_lti.name
//   project  = google_cloud_run_service.cc_lti.project
//   location = google_cloud_run_service.cc_lti.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_lti" {
  location = "us-central1"
  name     = "cc-lti.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_lti.project
  }

  spec {
    route_name = google_cloud_run_service.cc_lti.name
  }
}


#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."

resource "google_cloud_run_service" "cc_lti_qa" {
  provider = google-beta

  name                       = "cc-lti-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-lti/root:test"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_lti_qa_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
// resource "google_cloud_run_service_iam_policy" "cc_lti_qa" {
//   service  = google_cloud_run_service.cc_lti_qa.name
//   project  = google_cloud_run_service.cc_lti_qa.project
//   location = google_cloud_run_service.cc_lti_qa.location

//   policy_data = data.google_iam_policy.no_auth.policy_data
// }

resource "google_cloud_run_domain_mapping" "cc_lti_qa" {
  location = "us-central1"
  name     = "cc-lti-qa.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_lti_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_lti_qa.name
  }
}
