## Cloud Run

Terraform configuration for Cloud Run. This configuration handles

- Services on Cloud Run

### Note

Access for the secrets must be granted to the Cloud Run Service Account, remember to configure it in the IAM config.

The Artifact Registry config does not provide nice outputs for image links so manual linking is required.