#  ____________
# < PRODUCTION >
#  ------------
#         \   ^__^
#          \  (oo)\_______
#             (__)\       )\/\
#                 ||----w |
#                 ||     ||

# $$\   $$\  $$$$$$\  
# $$ |  $$ |$$  __$$\ 
# $$ |  $$ |$$ /  \__|
# $$ |  $$ |\$$$$$$\  
# $$ |  $$ | \____$$\ 
# $$ |  $$ |$$\   $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

resource "google_cloud_run_service" "cc_api_gateway_us" {
  provider = google-beta

  name                       = "cc-api-gateway-us"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api-gateway/root:latest"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_us_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
resource "google_cloud_run_service_iam_policy" "cc_api_gateway_us" {
  service  = google_cloud_run_service.cc_api_gateway_us.name
  project  = google_cloud_run_service.cc_api_gateway_us.project
  location = google_cloud_run_service.cc_api_gateway_us.location

  policy_data = data.google_iam_policy.no_auth.policy_data
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_us" {
  location = "us-central1"
  name     = "us.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_us.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_us.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_us_codecollab_io" {
  location = "us-central1"
  name     = "us.codecollab.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_us.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_us.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_us_global" {
  location = "us-central1"
  name     = "api.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_us.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_us.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_us_global_codecollab_io" {
  location = "us-central1"
  name     = "api.codecollab.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_us.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_us.name
  }
}

#   $$$$$$\   $$$$$$\  
# $$  __$$\ $$  __$$\ 
# $$ /  \__|$$ /  \__|
# \$$$$$$\  $$ |$$$$\ 
#  \____$$\ $$ |\_$$ |
# $$\   $$ |$$ |  $$ |
# \$$$$$$  |\$$$$$$  |
#  \______/  \______/

resource "google_cloud_run_service" "cc_api_gateway_sg" {
  provider = google-beta

  name                       = "cc-api-gateway-sg"
  location                   = "asia-southeast1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api-gateway/root:latest"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_sg_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
resource "google_cloud_run_service_iam_policy" "cc_api_gateway_sg" {
  service  = google_cloud_run_service.cc_api_gateway_sg.name
  project  = google_cloud_run_service.cc_api_gateway_sg.project
  location = google_cloud_run_service.cc_api_gateway_sg.location

  policy_data = data.google_iam_policy.no_auth.policy_data
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_sg" {
  location = "asia-southeast1"
  name     = "sg.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_sg.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_sg.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_sg_codecollab_io" {
  location = "asia-southeast1"
  name     = "sg.codecollab.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_sg.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_sg.name
  }
}




#  $$$$$$$$\ $$\   $$\ 
# $$  _____|$$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$\    $$ |  $$ |
# $$  __|   $$ |  $$ |
# $$ |      $$ |  $$ |
# $$$$$$$$\ \$$$$$$  |
# \________| \______/


resource "google_cloud_run_service" "cc_api_gateway_eu" {
  provider = google-beta

  name                       = "cc-api-gateway-eu"
  location                   = "europe-west4"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "production"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api-gateway/root:latest"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_eu_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
resource "google_cloud_run_service_iam_policy" "cc_api_gateway_eu" {
  service  = google_cloud_run_service.cc_api_gateway_eu.name
  project  = google_cloud_run_service.cc_api_gateway_eu.project
  location = google_cloud_run_service.cc_api_gateway_eu.location

  policy_data = data.google_iam_policy.no_auth.policy_data
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_eu" {
  location = "europe-west4"
  name     = "eu.cclb.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_eu.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_eu.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_eu_codecollab_io" {
  location = "europe-west4"
  name     = "eu.codecollab.io"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_eu.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_eu.name
  }
}

#                                  ----------------------------
#              | |                 |     Quality Assurance    |
#              | |===( )   //////  |  AKA I'M BREAKING STUFF  |
#              |_|   |||  | o o|  <----------------------------
#                     ||| ( c  )                  ____
#                      ||| \= /                  ||   \_
#                       ||||||                   ||     |
#                       ||||||                ...||__/|-\"
#                       ||||||             __|________|__
#                         |||             |______________|
#                         |||             || ||      || ||
#                         |||             || ||      || ||
# ------------------------|||-------------||-||------||-||-------
#                         |__>            || ||      || ||
# .================================================================."
resource "google_cloud_run_service" "cc_api_gateway_qa" {
  provider = google-beta

  name                       = "cc-api-gateway-qa"
  location                   = "us-central1"
  autogenerate_revision_name = true

  lifecycle {
    ignore_changes = [
      metadata.0.annotations,
    ]
  }

  metadata {
    labels      = {
      env = "qa"
    }
    annotations = {
      "run.googleapis.com/launch-stage" = "BETA"
    }
  }

  template {
    spec {
      container_concurrency = 1000

      containers {
        image = "us-central1-docker.pkg.dev/codecollab-io/cc-api-gateway/root:test"

        ports {
          container_port = 5000
        }

        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }

        resources {
          limits = {
            "cpu"    = 1
            "memory" = "128Mi"
          }
        }
      }

      volumes {
        name = "config"
        secret {
          secret_name = data.terraform_remote_state.secret_manager.outputs.cc_api_gateway_qa_config_secret_id
          items {
            key  = "latest"
            path = "config.toml"
          }
        }
      }

      service_account_name = data.terraform_remote_state.iam.outputs.cloud_run_account
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/minScale" = 0,
        "autoscaling.knative.dev/maxScale" = 1,
      }
    }
  }
}

# Allow unauthenticated invocations
resource "google_cloud_run_service_iam_policy" "cc_api_gateway_qa" {
  service  = google_cloud_run_service.cc_api_gateway_qa.name
  project  = google_cloud_run_service.cc_api_gateway_qa.project
  location = google_cloud_run_service.cc_api_gateway_qa.location

  policy_data = data.google_iam_policy.no_auth.policy_data
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_qa" {
  location = "us-central1"
  name     = "us.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_qa.name
  }
}

resource "google_cloud_run_domain_mapping" "cc_api_gateway_qa_global" {
  location = "us-central1"
  name     = "api.cclb.dev"

  metadata {
    namespace = google_cloud_run_service.cc_api_gateway_qa.project
  }

  spec {
    route_name = google_cloud_run_service.cc_api_gateway_qa.name
  }
}

