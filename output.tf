output "cc_api_name" {
  value = google_cloud_run_service.cc_api.name
}

output "cc_api_qa_name" {
  value = google_cloud_run_service.cc_api_qa.name
}

output "cc_auth_name" {
  value = google_cloud_run_service.cc_auth.name
}

output "cc_auth_qa_name" {
  value = google_cloud_run_service.cc_auth_qa.name
}

output "cc_editor_api_us_name" {
  value = google_cloud_run_service.cc_editor_api_us.name
}

output "cc_editor_api_eu_name" {
  value = google_cloud_run_service.cc_editor_api_eu.name
}

output "cc_editor_api_sg_name" {
  value = google_cloud_run_service.cc_editor_api_sg.name
}

output "cc_editor_api_qa_name" {
  value = google_cloud_run_service.cc_editor_api_qa.name
}

output "cc_lti_name" {
  value = google_cloud_run_service.cc_lti.name
}

output "cc_lti_qa_name" {
  value = google_cloud_run_service.cc_lti_qa.name
}

output "cc_api_location" {
  value = google_cloud_run_service.cc_api.location
}

output "cc_api_qa_location" {
  value = google_cloud_run_service.cc_api_qa.location
}

output "cc_auth_location" {
  value = google_cloud_run_service.cc_auth.location
}

output "cc_auth_qa_location" {
  value = google_cloud_run_service.cc_auth_qa.location
}

output "cc_editor_api_us_location" {
  value = google_cloud_run_service.cc_editor_api_us.location
}

output "cc_editor_api_eu_location" {
  value = google_cloud_run_service.cc_editor_api_eu.location
}

output "cc_editor_api_sg_location" {
  value = google_cloud_run_service.cc_editor_api_sg.location
}

output "cc_editor_api_qa_location" {
  value = google_cloud_run_service.cc_editor_api_qa.location
}

output "cc_lti_location" {
  value = google_cloud_run_service.cc_lti.location
}

output "cc_lti_qa_location" {
  value = google_cloud_run_service.cc_lti_qa.location
}
